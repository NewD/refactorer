const path = require('path')

module.exports = {
	entry: path.resolve(__dirname, 'index.js'),

	resolve: {
		fallback: {
			path: require.resolve('path-browserify'),
		},
	},
}
