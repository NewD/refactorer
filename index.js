const path = require('path')

const code = `
import React, { Component } from 'react'
import { Button, Col, Modal, Row, Select, Space, Typography, Table } from 'antd'
import { FilterOutlined, RedoOutlined, CheckOutlined } from '@ant-design/icons'
import { RegisterSample } from '@rcbi/ui-register-sample'
import { isActionAvailable, normalizeOperationsToOptions, normalizeToOptions } from './utils'
import { api } from '../../api'
import styles from './styles.css'
import { RenderTable } from '../../components/table'
import filter from 'lodash/filter'
import uniq from 'lodash/uniq'
import CloseIcon from '../../icons/CloseIcon'

export class BatchAccounting extends Component {
  constructor(props) {
    super(props)
    this.searchInput = React.createRef()
  }

  state = {
    permissions: [],
    operationPermittedOptions: [],
    selectedRows: [],

    isSpentOrIncome: false,
    selectedOperationPermitted: undefined,

    isOpenRegisterSample: false,

    filterBranch: undefined,
    filterBranchOptions: [],

    filterMaterial: undefined,
    filterMaterialOptions: [],

    filterOperation: undefined,
    filterOperationOptions: [],
    finalProcessingTypeOperation: undefined
  };

  componentDidMount() {
    this.init()
  }

  init = () => {
    api.getOperations().then(res => {
      const operationsPermitted = filter(res, (el) => (
        ['Packing', 'Fusion', 'Drilling', 'Ghd', 'Roasting'].includes(el.PermittedOperations)
      ))
      const finalProcessingTypeOperations = res && res.find((el) => (
        ['FinalProcessing'].includes(el.PermittedOperations)
      ))
      this.setState({
        operationPermittedOptions: normalizeOperationsToOptions(operationsPermitted),
        filterOperationOptions: normalizeOperationsToOptions(res),
        permissions: uniq(res.map(item => item.PermittedOperations)),
        finalProcessingTypeOperation: finalProcessingTypeOperations && finalProcessingTypeOperations.IdTypeOperationBatch
      })
    })

    api.getMaterials().then(res => {
      this.setState({
        filterMaterialOptions: normalizeToOptions(res)
      })
    })

    api.getOrgs().then(res => {
      this.setState({
        filterBranchOptions: normalizeToOptions(res)
      })
    })
  };

  handleReset = clearFilters => {
    clearFilters()
  };

  onFilterClear = () => {
    this.setState({
      filterBranch: undefined,
      filterMaterial: undefined,
      filterOperation: undefined
    })
  }

  onRegisterPropbe = () => {
    this.setState({
      isOpenRegisterSample: true
    })
  };

  onRegisterPropbeClose = () => {
    this.setState({
      isOpenRegisterSample: false
    })
  };

  onIssueTransportation = () => {
    const BatchIds = this.state.selectedRows.map(item => item.BatchIndex)
    return this.props.onAppOpen('batchmoving-form', { BatchIds })
  };

  onSpentOrIncome = () => {
    this.setState({
      isSpentOrIncome: true,
      selectedOperationPermitted: undefined
    })
  };

  closeSpentOrIncome = () => {
    this.setState({
      isSpentOrIncome: false,
      selectedOperationPermitted: undefined
    })
  };

  setSpentOrIncome = ({ value: OperationId }) => {
    const BatchIds = this.state.selectedRows.map(item => item.BatchIndex)
    const params = { OperationId, BatchIds }

    this.setState({
      isSpentOrIncome: false,
      selectedOperationPermitted: undefined
    })

    return this.props.onAppOpen('uiInOutGCP', params)
  };

  onCashProcess = () => {
    const BatchIds = this.state.selectedRows.map(item => item.BatchIndex)
    const { finalProcessingTypeOperation } = this.state

    return this.props.onAppOpen('accounting', {
      BatchIds,
      idTypeOperationBatch: finalProcessingTypeOperation
    })
  };

  onRefresh = () => this.setState({ Reload: {} });

  onFilterChange = (filter) => (value) => {
    this.setState({ [filter]: value })
  }

  onOperationPermittedChange = (value) => {
    this.setState({ selectedOperationPermitted: value })
  };

  handleChangeSelected = (selectedRows) => {
    this.setState({ selectedRows })
  };

  render() {
    const {
      operationPermittedOptions,
      selectedOperationPermitted,
      isSpentOrIncome,

      filterBranch,
      filterBranchOptions,

      filterMaterial,
      filterMaterialOptions,

      filterOperation,
      filterOperationOptions,

      selectedRows,
      isOpenRegisterSample,
      Reload,
      permissions
    } = this.state

    return (
      <div className={styles.container}>
        <div className={styles.filters}>
          <Space>
            <Typography.Text className={styles.title}>
              Выбор параметров
            </Typography.Text>
            <Select
              className={styles.select}
              placeholder='Отделение'
              allowClear
              value={filterBranch}
              onChange={this.onFilterChange('filterBranch')}
              options={filterBranchOptions}
            />
            <Select
              className={styles.select}
              placeholder='Материал'
              allowClear
              value={filterMaterial}
              onChange={this.onFilterChange('filterMaterial')}
              options={filterMaterialOptions}
            />
            <Select
              className={styles.select}
              placeholder='Последняя операция'
              allowClear
              value={filterOperation}
              onChange={this.onFilterChange('filterOperation')}
              options={filterOperationOptions}
            />
          </Space>
          <Space>
            <Button onClick={this.onFilterClear} className={styles.button}>
              Сбросить фильтр{' '}<FilterOutlined />
            </Button>
          </Space>
        </div>
        <div className={styles.content}>
          <Row justify='space-between'>
            <Col span={24}>
              <div className={styles.controls}>
                <Typography.Text className={styles.title}>
                  Запасы золотосодержащей продукции
                </Typography.Text>
                <Space>
                  {isActionAvailable('registerProbe', permissions) && (
                    <Button
                      onClick={this.onRegisterPropbe}
                      className={styles.button}
                      disabled={selectedRows.length !== 1 || selectedRows[0] && !selectedRows[0].IsSample}
                    >
                      Регистрация пробы
                    </Button>
                  )}
                  {isActionAvailable('issueTransportation', permissions) && (
                    <Button
                      onClick={this.onIssueTransportation}
                      className={styles.button}
                      disabled={selectedRows.length < 1}
                    >
                      Оформить перемещение
                    </Button>
                  )}
                  {isActionAvailable('spentOrIncome', permissions) && (
                    <Button
                      onClick={this.onSpentOrIncome}
                      className={styles.button}
                      disabled={selectedRows.length < 1}
                    >
                      Приход/Расход
                    </Button>
                  )}
                  {isActionAvailable('cashProcess', permissions) && (
                    <Button
                      onClick={this.onCashProcess}
                      className={styles.button}
                      disabled={selectedRows.length < 1}
                    >
                      Кассовая обработка
                    </Button>
                  )}
                  <Button onClick={this.onRefresh} className={styles.button} icon={<RedoOutlined />} />
                </Space>
              </div>
            </Col>
            <Col span={24}>
              <RenderTable
                MaterialId={filterMaterial} OrgId={filterBranch} OperationId={filterOperation}
                Reload={Reload} onChangeSelected={this.handleChangeSelected}
              />
            </Col>
          </Row>
        </div>
        {isOpenRegisterSample &&
          <RegisterSample
            isOpen={isOpenRegisterSample}
            handleClose={this.onRegisterPropbeClose}
            handleOk={() => {
            }}
            title='Регистрация пробы'
            formMode={1}
            source='Sample'
            {
              ...selectedRows[0] && !selectedRows[0].SampleBatch.Id
                ? {
                  
                } : {
                  sampleId: selectedRows[0].SampleBatch.Id
                }
            }
          />}
        <Modal
          className={styles.tableOperationModal}
          visible={isSpentOrIncome}
          title='Выбор операции'
          onCancel={this.closeSpentOrIncome}
          closeIcon={<CloseIcon />}
          footer={[
            <Button
              key='submit'
              type='primary'
              disabled={!selectedOperationPermitted}
              onClick={() => this.setSpentOrIncome(selectedOperationPermitted)}
            >
              Подтвердить <CheckOutlined />
            </Button>
          ]}
        >
          <Table
            rowKey='value'
            columns={[{
              title: 'Операции',
              dataIndex: 'label',
              width: 40
            }]}
            showHeader={false}
            bordered
            dataSource={operationPermittedOptions}
            pagination={false}
            size='small'
            scroll={{ y: 300 }}
            onRow={(record) => ({
              onClick: (event) => this.onOperationPermittedChange(record)
            })}
            rowSelection={{
              type: 'radio',
              columnWidth: 0,
              selectedRowKeys: selectedOperationPermitted ? [selectedOperationPermitted.value] : []
            }}
          />
        </Modal>
      </div>
    )
  }
}
`

function lexicalAnalysis(code) {
	const methods = []
	const state = {}

  const componentName = getComponentName(code)
  getComponentState(code)


	console.log(componentName)
}

function getComponentName(code) {
	return code
		.match(/class\s+\w+\s+extends\s+.*Component/gmi)[0]
		.trim()
		.split(' ')[1]
}

function getComponentMethods(code) {}

function getComponentState(code) {
  console.log(code.match(/\bstate\s*=\s*{/mg)[0])
}

lexicalAnalysis(code)
